class Card {
    constructor(id, name, email, title, body){
        this.id = id
        this.name = name;
        this.email=email;
        this.title=title;
        this.body=body;
    }

    renderCard(){
        let bodyId = document.getElementById('bodyId');
        bodyId.insertAdjacentHTML('beforeend', `<div id="card_${this.id}" style="border: 3px solid #00008B; border-radius: 5px; background-color: #F0FFFF; padding: 15px 20px; margin-bottom: 10px">
        <span style="font-size: 24px">${this.name}</span>,
        <span style="color: blue"> ${this.email}</span>
        <h4 style="margin: 10px 0px; color:#B0C4DE">${this.title}</h4>
        <p>${this.body}</p>
        <button id="btn_${this.id}" style="padding: 10px; color: #00008B;">Delete</button>
        </div>`);

        let btnId = document.getElementById("btn_"+this.id);
        let card = this;
        btnId.addEventListener("click", function(){
            card.deleteCard();
        });
    }

    deleteCard(){
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`,{
                method: 'DELETE',
            })
            .then ( (res) => {
                if (res.ok) {
                    document.getElementById(`card_${this.id}`).remove();                    
                } else { 
                    console.log("KAKASHKA")
                }
            })     

    }
}

function createCardsList(users, posts){
    for (let post of posts) {
        let user = users.find(user=>user.id == post.userId);
            let card = new Card (post.id, user.name+' '+user.username, user.email, post.title, post.body)
            card.renderCard()
    }
}

function loadData() {
    fetch('https://ajax.test-danit.com/api/json/users')
    .then((res) => res.json())
    .then((users) => {
        fetch('https://ajax.test-danit.com/api/json/posts')
            .then((res) => res.json())
            .then((posts) => {
                console.log(posts)
                createCardsList(users, posts);
            })
        }
    )

}

loadData();